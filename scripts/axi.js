/*Copyright(C) 2010-2013 Isaiah Gilliland
*
* This file is part of Axis.
*
*    Axis is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Lesser General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Axis is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public License
*    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
*/

// Axis AI engine. One great big awesome behavior tree,
// but it's very light, basically an data structure to make your own

axi = {
    name: "root",
    b:{}
    };

axi.runThrough = function(actor, place){
    //plumbing
    if(!place){
        place = axi;
    }
    
    for(var i in place.b){
        //make sure to only process the behaviors
        if(place.b[i].axisBehavior){
            //if it returns true continue down its tree
            if(place.b[i].start(actor, place.b[i].data)){
                axi.runThrough(actor, place.b[i]);
            }
        }
    }
};

/*
* The basic behavior creator. Must be supplied at least a name and a
* starting function that checks a condition and returns either true/false
* if you do not have it return true or false the system will assume its 
* condition failed as undefined is also false. 
*
* If a place is not supplied it will assume it should place it in the root
* of the tree. You specify the place by actually passing the behavior it will
* branch from. Example:
*
* axi.addBehavior({
*    name:run,
*    start:function(act){
*       if(axu.distance(act.pos,main.pos) >= 100){
*           act.moveto(main.pos);
*           return true;
*       }else{
*           return false;
*       }
*    }
* });
*/
axi.addBehavior = function(be){
    //plumbing
    if(!be.place){
        be.place = axi;
    }
    if(!be.name){
        throw "axi.addBehavior: Must supply behavior name";
    }
    if(!be.start){
        throw "axi.addBehavior: Must supply a starting condition";
    }
    
    //creating behavior base
    var newBehavior = {
        name : be.name,
        start : be.start,
        finish : be.finish,
        data : be.data || {},
        axisBehavior : true,
        b:{}
    };
    //placing it
    be.place.b[be.name] = newBehavior;
};


axt = {};
axt.b = {};
axt.layer = 3;
axt.Node = function(name, initObject) {
	var self = this;
	var act = null;
	
	self.text = initObject.text || "dialogue";
	self.pos = initObject.pos || [0, 0];
	self.width = initObject.width || 200;
	self.height = initObject.height || 100;
	self.txtOffset = initObject.txtOffset || [10, 10];
	self.txtWidth = initObject.txtWidth || self.width - 20;
	self.txtHeight = initObject.txtHeight || self.height - 20;
	self.test = initObject.test || null;
	self.label = initObject.label || null;//not really used yet
	self.showChoices = initObject.showChoices || false;//not used yet
	self.dynamicDraw = initObject.dynamicDraw || null;//not used yet
	self.stamp = initObject.stamp || null;
	self.animation = initObject.animation || null;
	self.color = initObject.color || "rgb(250,250,250)";
	self.font = initObject.font || "serif";
	self.fontSize = initObject.fontSize || 10;
	self.fontWeight = initObject.fontWeight || "";
	self.lineSpace = initObject.lineSpace || self.fontSize*1.5;
	self.bttnText = initObject.bttnText || "Continue";
	self.bttnWeight = initObject.bttnWeight || self.fontWeight;
	self.bttnSize = initObject.bttnSize || self.fontSize;
	self.bttnFont = initObject.bttnFont || self.font;
	self.bttnColor = initObject.bttnColor || self.color;
	self.bttnHColor = initObject.bttnHColor || "rgb(150,100,0)";
	self.typeSpeed = initObject.typeSpeed || null;
	self.typeSound = initObject.typeSound || null;
	self.pause = initObject.pause || false;
	self.fixed = initObject.fixed || false;
	self.tag = initObject.tag || "";
	//in duration sometimes a null value is desired
	if(initObject.duration === undefined) {
		self.self.duration = 5;
	} else {
		self.duration = initObject.duration;
	}

	self.actor = null;
	var parent = null;
	var b = [];
	self.b = {};
	//adding events
	axe.addEventsTo(self);
	if(initObject.action)self.addEvent("action", initObject.action);
	if(initObject.endAction)self.addEvent("endAction", initObject.endAction);

	//some methods
	self.addNode = function(initObject, name) {
		initObject.text = initObject.text || self.text;
		initObject.pos = initObject.pos || self.pos;
		initObject.width = initObject.width || self.width;
		initObject.height = initObject.height || self.height;
		initObject.txtOffset = initObject.txtOffset || self.txtOffset;
		initObject.txtWidth = initObject.txtWidth || self.txtWidth;
		initObject.txtHeight = initObject.txtHeight || self.txtHeight;
		initObject.test = initObject.test || self.test;
		initObject.action = initObject.action || self.action;
		initObject.label = initObject.label || self.label;
		initObject.showChoices = initObject.showChoices || self.showChoices;
		initObject.dynamicDraw = initObject.dynamicDraw || self.dynamicDraw;
		initObject.stamp = initObject.stamp || self.stamp;
		initObject.animation = initObject.animation || self.animation;
		initObject.color = initObject.color || self.color;
		initObject.font = initObject.font || self.font;
		initObject.fontSize = initObject.fontSize || self.fontSize;
		initObject.fontWeight = initObject.fontWeight || self.fontWeight;
		initObject.lineSpace = initObject.lineSpace || self.lineSpace;
		initObject.typeSpeed = initObject.typeSpeed || self.typeSpeed;
		initObject.typeSound = initObject.typeSound || self.typeSound;
		initObject.pause = initObject.pause || self.pause;
		initObject.fixed = initObject.fixed || self.fixed;
		initObject.bttnText = initObject.bttnText || self.bttnText;
		initObject.bttnWeight = initObject.bttnWeight || self.bttnWeight;
		initObject.bttnSize = initObject.bttnSize || self.bttnSize;
		initObject.bttnFont = initObject.bttnFont || self.bttnFont;
		initObject.bttnColor = initObject.bttnColor || self.bttnColor;
		initObject.bttnHColor = initObject.bttnHColor || self.bttnHColor;
		initObject.tag = initObject.tag || self.tag;
		if(initObject.duration === undefined)
			initObject.duration = self.duration;

		var node = new axt.Node(false, initObject);
		//give the user the ability to also put the node in an object
		//for easy access later.
		if(name) {
			self.b[name] = node;
		}
		b.push(node);
		return node;
	};

	self.next = (function() {
		var length = 0;
		var place = 0;

		return function(data) {
			length = b.length;
			if(self.actor) {
				self.actor.killAll();
				self.actor = null;
			}
			if(length === 0 || place >= length) {
				place = 0;
				self.fireEvent("endAction", data);
				if(parent) {
					parent.next(data);
					parent = null;
				} else if(node.pause) {
					ax.unPauseGameNotMusic();
				}
				return;
			}

			if(b[place]) {
				b[place].fire(self, data);
			}
			place++;

		}
	})();

	self.fire = function(caller, data) {
		parent = caller;
		if(!self.test || self.test()) {
			self.fireEvent("action", data);
			if(self.duration) {
				var durationScpt = new ax.Scripter();
				durationScpt.cmd(self.duration, function() {
					self.next();
				});
				durationScpt.fire()
			}
			var act;
			//impliment text typing
			if(self.typeSpeed) {
				var buffer = "";
				//if there is typing, duration must be suspended
				if(durationScpt)
					durationScpt.onOff();

				var place = 0;
				var typeScpt = new ax.Scripter;
				typeScpt.loop = true;

				typeScpt.cmd((11 - self.typeSpeed) / ax.fps, function() {
					if(place >= self.text.length) {
						typeScpt.loop = false;
						typeScpt.kill();
						if(durationScpt)
							durationScpt.onOff();
						return;
					}
					if(self.typeSound)
						axr.audio[self.typeSound].play();
					buffer += self.text[place];
					act.text.txt = buffer;
					place++;
				});
				typeScpt.fire();
			} else {
				var buffer = self.text;
			}
			node = self;
			if(node.pause)
				ax.pauseGameNotMusic();
			node.pos = (parent)? parent.pos: node.pos;
			self.actor = act = new ax.Actor({
				type : "dialogue",
				pos : node.pos,
				width : node.width,
				height : node.height,
				stamp : node.stamp,
				aniName : node.animation,
				layer : axt.layer,
				fixed : self.fixed,
				pauseFree : node.pause
			});
			//setting the dialogue text
			act.text.txt = buffer;
			act.text.color = node.color;
			act.text.offset = node.txtOffset;
			act.text.width = node.txtWidth;
			act.text.height = node.txtHeight;
			act.text.font = node.font;
			act.text.size = node.fontSize;
			act.text.spacing = node.lineSpace;
			act.buttons = [];
			//a clean function to make sure everything is removed
			//from the past node before moving forward, such as scripters
			act.killAll = function() {
				if(typeScpt)
					typeScpt.kill();
				if(durationScpt)
					durationScpt.kill();
				act.kill();
				for(x in act.buttons) {
					if(act.buttons[x].kill)
						act.buttons[x].kill();
				}
			};

			ax.ctx.save();
			ax.ctx.font = self.bttnWeight + " " + self.bttnSize + "pt " + self.bttnFont;
			var bttnWidth = ax.ctx.measureText(node.bttnText).width + 1;
			var bttnHeight = node.bttnSize + 8;
			var cont = new ax.Actor({
				type : "button",
				pos : [node.pos[0] + (node.txtOffset[0] - node.txtWidth / 2 + bttnWidth), node.pos[1] + (node.txtOffset[1] + node.txtHeight / 2 + bttnHeight / 2)],
				width : bttnWidth,
				height : bttnHeight,
				layer : axt.layer,
				fixed : node.fixed
			});
			ax.ctx.restore();
			act.buttons.push(cont);
			cont.text.txt = node.bttnText;
			cont.text.font = node.bttnFont;
			cont.text.size = node.bttnSize;
			cont.text.weight = node.bttnWeight;
			cont.text.color = node.bttnColor;

			cont.addEvent("draw", function() {
				if(cont.mhover) {
					cont.text.color = node.bttnHColor;
				} else {
					cont.text.color = self.bttnColor;
				}
			});

			cont.addEvent("leftUp", function() {
				self.next(data);
				if(durationScpt) {
					durationScpt.kill();
				}
			});

		} else {
			//must embed call within a scripter to keep from exceeding
			//call stack
			var nextScpt = new ax.Scripter();
			nextScpt.cmd(0, function() {
				self.next(data);
			});
			nextScpt.fire()
		}
	};

	if(name) {
		axt.b[name] = self;
	}

	return self;
}; 


function testDialog (){    
    new axt.Node("test",{
        width:463,
        height:163,
        pos:[ax.halfWidth,ax.halfHeight],
        fontSize:16,
        txtOffset:[59,-22],
        txtWidth:318,
        color: "black",
        bttnColor: "rgb(0,200,0)",
        bttnHColor: "rgb(200,0,0)",
        bttnFont: "Arial",
        bttnWeight: "bold",
        typeSpeed: 10,
        duration:2,
        bttnSize:15,
        fixed:true,
        bttnText:"Next",
        text:"Wonderful!! This is dialogue branch number 1. It is a branch "+
        "because it starts a whole tree of dialogue. It is purposely long "+
        "to show off word wrapping in action."
    });
    
    axt.b.test.addNode({label:"diag2",text:"This is dialogue node number 2"},"diag2")
    
    axt.b.test.b.diag2.addNode({label:"diag3",text:"This is a dialogue node in a branch of diag2"});
    
    axt.b.test.b.diag2.addNode({
        label:"diag3",
        text:"This is also in the branch of diag2, but it shouldn't show"+
        " because it has a test that is always false.",
        test: function(){ return false}
    });
    
    axt.b.test.addNode({
        label:"diag3",
        text:"This is dialogue node number 3. It also has an action "+
        "that is executed when it is run.",
        action: function(){console.log("hey from dialogue node number 3!")}
    },"diag3");
    
    axt.b.test.fire();
}
