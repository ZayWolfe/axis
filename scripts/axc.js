/*Copyright(C) 2010-2013 Isaiah Gilliland
*
* This file is part of Axis.
*
*    Axis is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Lesser General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Axis is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public License
*    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
*/

var axc = {};

axc.init = function(){
	/*axc.canvas = document.createElement('canvas');
	axc.ctx = axc.canvas.getContext('2d');*/
	axc.collisions = [];
	
};

axc.addCollider = function(actor){//give uncentered position
	var obj = {
		pos:actor.realPosition(),
		speed:[],
		rad:actor.collideRadius,
		poly:actor.collidePoly,
		actor:actor,
		collList:[]
	};
	obj.isColliding = (function(i){
		
		return function(base){
			i = obj.collList.length;
			while(i--){
				if(obj.collList[i] === base){
					return true;
				}
			}
			return false;
		};
	})();
	obj.removeColl = (function(i,l,nArray){

		return function(base){
			l = obj.collList.length;
			i = 0;
			nArray = [];
			while(i<l){
				if(obj.collList[i] !== base){
					nArray.push(obj.collList[i]);
				}
				i++;
			}
			obj.collList = nArray;
		};
	})();
	return axc.collisions.push(obj)-1;
};

axc.checkCollision = (function(){
	var l, i, ni, dis;
	var ob1,ob2;
	
	return function(){
		l = axc.collisions.length;
		i = 0;

		while(i < l){
			ni=i+1;
			ob1 = axc.collisions[i++];
			if(!ob1)continue;
			
			while(ni < l){
				ob2 = axc.collisions[ni++];
				if(!ob2)continue;
                
				var answer = axc.collisionTest(ob1,ob2);
				
				if(ob1.isColliding(ni)){
					if(!answer){
						ob1.removeColl(ni);
					}else{
						ob1.actor.fireEvent("colliding",
											{
												actor:ob2.actor,
												self:ob1.actor,
												intercept: answer[0],
												wall: answer[1]
											});
						ob2.actor.fireEvent("colliding",
											{
												actor:ob1.actor,
												self:ob2.actor,
												intercept: answer[0],
												wall: answer[1]
											});
					}
				}else{
					if(answer){
						ob1.collList.push(ni);
						ob1.actor.fireEvent("collide",
											{
												actor:ob2.actor,
												self:ob1.actor,
												intercept: answer[0],
												wall: answer[1]
											});
						ob2.actor.fireEvent("collide",
											{
												actor:ob1.actor,
												self:ob2.actor,
												intercept: answer[0],
												wall: answer[1]
											});

					}
				}
				

			}
			
		}
	};
		
})();

axc.collisionTest = function (ob1,ob2) {
		if(!ob1.poly && !ob2.poly){
			return axc.sphereToSphere(ob1,ob2);
		} else if(ob1.poly && !ob2.poly){
			return axc.sphereToPoly(ob2,ob1);
		} else if(!ob1.poly && ob2.poly){
			return axc.sphereToPoly(ob1,ob2);
		} else if(ob1.poly && ob2.poly){
			return axc.polyToPoly(ob1,ob2);
		}

};

axc.sphereToSphere = function(ob1, ob2){
	var dis = Math.sqrt(Math.pow((ob1.pos[0]-ob2.pos[0]),2)+
							Math.pow((ob1.pos[1]-ob2.pos[1]),2));
	
	if(ob1.rad+ob2.rad >= dis){
		var n = axu.getNormal([ob2.pos[0]-ob1.pos[0],ob2.pos[1]-ob1.pos[1]]);
		var inter = [n[0]*ob1.rad,n[1]*ob1.rad];
		return axc.makeCollisionReturn(ob1.pos,inter);
	}else {
		return false;
	}
}

axc.sphereToPoly = function(objS, objP){
	var poly = axu.offsetPoly(objP.poly, objP.pos);
	var width = axu.polyWidth(poly);
	var height = axu.polyHeight(poly);
	var length = poly.length;
	
	var pos = objS.pos;
	var radius = objS.rad;
	
	var collision = false;
	
	//first check to see if they're anywhere near each other!!
	if(axu.distance(pos,objP.pos) <=
	   axu.getLength([width,height]) + radius){
		//check points for collision
		for(var i = 0; i < length; i++){
			var dis = axu.distance(pos, poly[i]);
			if(dis <= radius){
				collision = poly[i];
			}
		}
		if(!collision){
			collision = axc.linePolyIntersect(pos, objP.pos, poly);
			
		} else {
			collision = axc.makeCollisionReturn(pos,collision);
		}/**/
	}
	
	return collision;		
};

axc.polyToPoly = function(obj1, obj2){
	var poly1 = axu.offsetPoly(obj1.poly, obj1.pos);
	var poly2 = axu.offsetPoly(obj2.poly, obj2.pos);
	var width1 = axu.polyWidth(poly1);
	var height1= axu.polyHeight(poly1);
	var width2 = axu.polyWidth(poly2);
	var height2= axu.polyHeight(poly2);
	
	var length;
	var coord;
	var collision = false;
	
	//first check to see if they're anywhere near each other!!
	if(axu.distance(obj1.pos,obj2.pos) <=
			axu.getLength([width1,height1]) +
			axu.getLength([width2,height2])){
		
		//check points for collision with first poly
		length = poly1.length;
		for(var i = 0; i < length; i++){
			coord = poly1[i];
			if(axu.inpoly(poly2, coord[0], coord[1])){
				collision = coord;
				break;
			}
		}
		if(!collision){//move onto second test, if not found in first
			//check points for collision with second poly
			length = poly2.length;
			for(var i = 0; i < length; i++){
				coord = poly2[i];
				if(axu.inpoly(poly1, coord[0], coord[1])){
					collision = coord;
					break;
				}
			}
			if(collision){
				///if it found a collision with the second test get the return data
				collision = axc.makeCollisionReturn(obj2.pos,collision);
			}
		} else {
			//if it found a collision with the first test get the return data
			collision = axc.makeCollisionReturn(obj1.pos,collision);
		}/**/
	}
	
	return collision;		
};

axc.makeCollisionReturn = function(orig, inter){
	var n = axu.getNormal([inter[0]-orig[0],inter[1]-orig[1]]);
	return [
			inter,
			[[inter[0]+(-n[1]*10), inter[1]+(n[0]*10)],
			[inter[0]+(n[1]*10), inter[1]+(-n[0]*10)]]];
};

axc.pathPolyCheck = function(act,vel){
	var radius = (act.pathCollisionRadius)? act.pathCollisionRadius:
				axu.getLength([act.width, act.height])/2;
	var begin = [act.pos[0], act.pos[1]];
	var n = act.normal;
	var out = false;
	var offset = 1;
	var end = [
		begin[0]+n[0]*(radius*2)+act.vel[0],
		begin[1]+n[1]*(radius*2)+act.vel[1]
	];

	var result = axc.linePolyIntersect(begin,end, act.path);
	
	//calculate normal displace
	if(result){
		var intercept = result[0];
		var distance = axu.distance(begin, intercept);
		if(distance <= radius){
			out = result;
			var displace = Math.abs(radius - distance + offset)*-1;
			
			vel = [vel[0]+n[0]*displace, vel[1]+n[1]*displace];
		}
	}
	//calculate right normal displace
	n = act.rightNormal;
	end = [
		begin[0]+n[0]*(radius*2)+act.vel[0],
		begin[1]+n[1]*(radius*2)+act.vel[1]
	];
	
	result = axc.linePolyIntersect(begin, end, act.path);
	
	if(result){
		var intercept = result[0];
		var distance = axu.distance(begin, intercept);
		if(distance <= radius){
			out = result;
			var displace = Math.abs(radius - distance + offset)*-1;
			
			vel = [vel[0]+n[0]*displace, vel[1]+n[1]*displace];
		}
	}
	//calculate left normal displace
	n = act.leftNormal;
	end = [
		begin[0]+n[0]*(radius*2)+act.vel[0],
		begin[1]+n[1]*(radius*2)+act.vel[1]
	];
	
	result = axc.linePolyIntersect(begin, end, act.path);
	
	if(result){
		var intercept = result[0];
		var distance = axu.distance(begin, intercept);
		if(distance <= radius){
			out = result;
			var displace = Math.abs(radius - distance + offset)*-1;
			
			vel = [vel[0]+n[0]*displace, vel[1]+n[1]*displace];
		}
	}
	//calculate back normal displace
	n = [-act.normal[0], -act.normal[1]];
	end = [
		begin[0]+n[0]*(radius*2)+act.vel[0],
		begin[1]+n[1]*(radius*2)+act.vel[1]
	];
	
	result = axc.linePolyIntersect(begin, end, act.path);
	
	if(result){
		var intercept = result[0];
		var distance = axu.distance(begin, intercept);
		if(distance <= radius){
			out = result;
			var displace = Math.abs(radius - distance + offset)*-1;
			
			vel = [vel[0]+n[0]*displace, vel[1]+n[1]*displace];
		}
	}
	
	//check if event must be triggered
	if(out){
		if(act._domoveout){
			act._domoveout = false;
			act.fireEvent("moveout", {intercept:out[0],wall:out[1]});
		} else {
			act._domoveout = true
		}
		act.fireEvent("movingout", {intercept:out[0],wall:out[1]});
	}
	
	return vel;
};
axc.linePolyIntersect = function (L1,L2,poly){
    var result = false
    for(var i = 0,l = poly.length,vert; i<l; i++){
		var point1 = poly[i];
		var point2 = poly[i+1];
	    if(!point2)point2 = poly[0];
        vert = axc.lineIntersect(L1,L2,point1,point2);
	
        if(vert){
            result =  [vert,[point1, point2]];
		    break;
        }
    }
    return result;
};
axc.lineIntersect = function(a1, a2, b1, b2) {
    var result = false;
    
    var ua_t = (b2[0] - b1[0]) * (a1[1] - b1[1]) - (b2[1] - b1[1]) * (a1[0] - b1[0]);
    var ub_t = (a2[0] - a1[0]) * (a1[1] - b1[1]) - (a2[1] - a1[1]) * (a1[0] - b1[0]);
    var u_b  = (b2[1] - b1[1]) * (a2[0] - a1[0]) - (b2[0] - b1[0]) * (a2[1] - a1[1]);

    if ( u_b != 0 ) {
        var ua = ua_t / u_b;
        var ub = ub_t / u_b;

        if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
            //they intersect
            result = [a1[0] + ua * (a2[0] - a1[0]),a1[1] + ua * (a2[1] - a1[1])];
        } else {
            result = false;
        }
    } else {
        if ( ua_t == 0 || ub_t == 0 ) {
            result = false;
        } else {
            result = false;
        }
    }

    return result;
};
