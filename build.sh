#!/bin/bash

coffee -c scripts/*.coffee &&
java -jar compiler.jar --js=scripts/axis.js --js=scripts/axc.js --js=scripts/axd.js --js=scripts/axe.js --js=scripts/axi.js --js=scripts/axr.js --js=scripts/axu.js --js_output_file ./axis-compiled.js 